var FizzBuzzModule = (function() {
    function FizzBuzz() {
      this.num = 100;
      this.output = '';
      this.checks = [
        { 
          divider: 3, 
          output: 'fizz' 
        },
        { 
          divider: 5, 
          output: 'buzz' }
      ];
    }
  
    FizzBuzz.prototype.fizzbuzz = function() {   
      for (var i = 0 + 1; i <= this.num; i++) {
        for (var j = 0; j < this.checks.length; j++) {
          var check = this.checks[j];
          if (i % check.divider === 0) {
            this.output += check.output;
          }
        }
        this.output += ' ';
      }
      return this.output.split(' ').filter(function(num) { return num !== '' });
    }
  
    return FizzBuzz;
  })();
  
  
  describe("FizzBuzz", function() {
      it("A fizz buzz array is expected", function() {
          var component = new FizzBuzzModule();
          expect(component.fizzbuzz()).toEqual(["fizz", "buzz", "fizz", "fizz", "buzz", "fizz", "fizzbuzz", "fizz", "buzz", "fizz", "fizz", "buzz", "fizz", "fizzbuzz", "fizz", "buzz", "fizz", "fizz", "buzz", "fizz", "fizzbuzz", "fizz", "buzz", "fizz", "fizz", "buzz", "fizz", "fizzbuzz", "fizz", "buzz", "fizz", "fizz", "buzz", "fizz", "fizzbuzz", "fizz", "buzz", "fizz", "fizz", "buzz", "fizz", "fizzbuzz", "fizz", "buzz", "fizz", "fizz", "buzz"]);
      });
  });